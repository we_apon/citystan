﻿using System;
using System.IO;
using System.Linq;

namespace Citystan.Randomizer
{
    internal class Program
    {
        private static byte _separator = (byte)',';
        private static Random _random = new Random();

        private static void Main(string[] args) {
            var path = args.Any() ? args[0] : AppDomain.CurrentDomain.BaseDirectory;
            if (Directory.Exists(path))
                path += "//cities.txt";

            if (File.Exists(path))
                File.Delete(path);

            var max = ushort.MaxValue;
            var current = 0;

            using (var file = File.OpenWrite(path)) {
                while (++current < max) {
                    var word = RandomAsciiWordWithSeparator();
                    file.Write(word, 0, word.Length);
                }

                file.Flush();
            }
        }



        private static byte[] RandomAsciiWordWithSeparator() {
            var length = _random.Next(3, 18);
            var word = new byte[length + 1];

            for (var i = 0; i < length; i++) {
                word[i] = (byte)_random.Next(224, 256); // lowercase russian letters in 1251-codepage
            }

            word[length] = _separator;
            return word;
        }

    }

}