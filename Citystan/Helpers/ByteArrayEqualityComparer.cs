using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Citystan.Helpers
{
    class ByteArrayEqualityComparer : IEqualityComparer<byte[]>
    {
        /// <summary>
        /// Method from C runtime. May required vcredist
        /// </summary>
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int memcmp(byte[] b1, byte[] b2, long count);

        public bool Equals(byte[] x, byte[] y) {
            return x.Length == y.Length && memcmp(x, y, y.Length) == 0;
        }

        public int GetHashCode(byte[] obj) {
            var result = 13*obj.Length;
            for (var i = 0; i < obj.Length; i++) {
                result = (17*result) + obj[i];
            }
            return result;
        }
    }
}