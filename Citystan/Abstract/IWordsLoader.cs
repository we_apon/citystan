using System.Collections.Generic;
using Citystan.Entities;

namespace Citystan.Abstract
{
    public interface IWordsLoader
    {
        void LoadInto(IDictionary<byte, WordList> words, IList<WholeWord> repository);
    }
}