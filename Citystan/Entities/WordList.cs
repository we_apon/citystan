﻿using System.Collections.Generic;

namespace Citystan.Entities
{
    /// <summary>
    /// Containing all words, that starts on some letter
    /// </summary>
    public class WordList
    {
        public ushort Count;
        
        private readonly IDictionary<byte, TrickyWord> _trickyWords = new Dictionary<byte, TrickyWord>(33);
//        private readonly BitArray _isInitialized = new BitArray(255);


//        private readonly TrickyWord[] _trickyWords = new TrickyWord[255];
//        private readonly HashSet<byte> _lastLetters = new HashSet<byte>(); 


        private class TrickyWord
        {
            private readonly ushort[] _indexes;

            public ushort Count;

            public TrickyWord(byte unused) {
                _indexes = new ushort[100];
                Count = 0;
            }

            public ushort TakeSome() {
                return _indexes[--Count];
            }

            public void Add(ushort index) {
                _indexes[Count++] = index;
            }
        }

        /// <summary>
        /// Returns distinct last letters
        /// </summary>
        public virtual IEnumerable<byte> LastLetters() {
            return _trickyWords.Keys;
//            return _lastLetters;
        }


        /// <summary>
        /// Returns index of some word, that ends on <paramref name="lastLetter"/>
        /// </summary>
        public virtual ushort GiveMeWordOn(byte lastLetter) {
            Count--;

            var word = _trickyWords[lastLetter];
            var index = word.TakeSome();
            if (word.Count == 0)
                _trickyWords.Remove(lastLetter);
                //_lastLetters.Remove(lastLetter);

            return index;
        }


        /// <summary>
        /// Adds word, that ends with <paramref name="lastLetter"/> and has specified <paramref name="index"/> to current <see cref="WordList"/>
        /// </summary>
        public void Add(byte lastLetter, ushort index) {
            Count++;

/*            var word = _trickyWords[lastLetter];
            if (word == null) {
                word = new TrickyWord(lastLetter);
                _trickyWords[lastLetter] = word;
                _lastLetters.Add(lastLetter);
            }
            word.Add(index);*/

            TrickyWord word;
            if (_trickyWords.TryGetValue(lastLetter, out word))
                _trickyWords[lastLetter].Add(index);
            else {
                word = new TrickyWord(lastLetter);
                word.Add(index);

                _trickyWords[lastLetter] = word;
                
            }

        }
    }


    
}