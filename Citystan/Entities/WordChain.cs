﻿using System;
using System.IO;
using System.Threading.Tasks;
using Citystan.Services;

namespace Citystan.Entities
{
    /// <summary>
    /// Chain of words
    /// </summary>
    public class WordChain : IDisposable
    {
        private readonly WordRepository _repository;
        private ushort _length;
        private FileStream _file;
        private Task _writeTask;

        public const byte Separator = (byte)'.';

        /// <summary>
        /// "cities-chain.txt"
        /// </summary>
        public const string DefaultOutputPath = "cities-chain.txt";

        public WordChain(WordRepository repository, string outputPath = DefaultOutputPath) {
            _repository = repository;

            _writeTask = Task.Run(() => {
                if (File.Exists(outputPath))
                    File.Delete(outputPath);

                _file = File.OpenWrite(outputPath);
            });
        }

        /// <summary>
        /// Adds some words, by it's <paramref name="index"/>, to the chain
        /// </summary>
        public virtual void Add(ushort index) {
            _writeTask.Wait();

            var word = _repository.WordAt(index);
            _file.Write(word.Bytes, 0, word.Length);
            _file.WriteByte(Separator);
            _length++;
        }


        /// <summary>
        /// Returns current length of chain
        /// </summary>
        public virtual ushort Length() {
            return _length;
        }

        public void Dispose() {
            _file.Dispose();
        }
    }
}