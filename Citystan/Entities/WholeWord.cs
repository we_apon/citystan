namespace Citystan.Entities
{

    public struct WholeWord
    {
        public byte[] Bytes;
        public byte Length;

        public WholeWord(byte[] bytes, byte length) {
            Bytes = bytes;
            Length = length;
        }
    }
}