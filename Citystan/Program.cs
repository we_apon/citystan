﻿
using System;
using System.Linq;
using Citystan.Entities;
using Citystan.Services;

namespace Citystan
{
    public class Program
    {
        public static void Main(string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Укажите путь к текстовому файлу.\n" +
                                  "По желанию, вторым аргументом можете указать один символ, разделяющий слова (по умолчанию - ',')\n" +
                                  "Третьим аргументом можете указать путь к выходному файлу.");
                return;
            }

            var separator = args.Length > 1
                ? (byte)args[1].First()
                : (byte) ',';

            var output = args.Length > 2
                ? args[2]
                : WordChain.DefaultOutputPath;

            var loader = new FileWordsLoader(args[0], separator);
            var repository = new WordRepository(loader);
            using (var chain = new WordChain(repository, output)) {
                var finder = new ChainFinder(repository, chain);
                finder.FindChain(10000);
            }
        }

    }

}
