using System.Linq;
using Citystan.Entities;

namespace Citystan.Services
{
    public class ChainFinder
    {
        private readonly WordRepository _repository;
        private readonly WordChain _chain;

        public ChainFinder(WordRepository repository, WordChain chain) {
            _repository = repository;
            _chain = chain;
        }


        public void FindChain(int length) { //todo: check for size of unique words
            byte firstLetter;

            do {
                firstLetter = _repository.AvailableFirstLetters.FirstOrDefault();
            } while (firstLetter == default(byte));


            do {
                // �������� ��������� ����� �� ������ �����
                var possibleWords = _repository.WordsOn(firstLetter);

                // �������� �� ���� ���� ��������� ���������
                var possibleNextLetters = possibleWords.LastLetters();

                // �������� �� ���
                var enumerator = possibleNextLetters.GetEnumerator();
                enumerator.MoveNext();

                // ��� ������, ��� �������� �������� ��������� �����
                while (_repository.WordsCountOn(enumerator.Current) < 2) {
                    // ����� �������� ���� �����, � ������� ��������� ����� ����� ��, ��� ������
                    enumerator.MoveNext();
                }

                // �������� �������� ������ ���������� �����, �������������� �� ���������� �����
                var index = possibleWords.GiveMeWordOn(enumerator.Current);

                // ��������� ������ ���������� ����� � �������
                _chain.Add(index);

                // ������ ��������� ����� �������� ����� - ������ ����������
                firstLetter = enumerator.Current;
            } while (_chain.Length() < length);
        }
    }
}