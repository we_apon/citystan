using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Citystan.Abstract;
using Citystan.Entities;

namespace Citystan.Services
{
    public class FileWordsLoader : IWordsLoader
    {
        private readonly string _filePath;
        private readonly byte _separator;
        private byte[] _bytes;
        private Task _readAllTask;

        public FileWordsLoader(string filePath, byte separator) {
            _filePath = filePath;
            _separator = separator;
            _readAllTask = Task.Run(new Action(ReadAllBytes));
        }

        private void ReadAllBytes() {
            _bytes = File.ReadAllBytes(_filePath);
        }






        private class TrickyByteArrayEqualityComparer : IEqualityComparer<byte[]>
        {
            [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
            private static extern int memcmp(byte[] b1, byte[] b2, long count);

            public bool Equals(byte[] x, byte[] y) {
                return memcmp(x, y, _currentLength) == 0; // this is, actually, should not be used in HashSet, but..
            }

            public int GetHashCode(byte[] obj) {
                var result = 13*_currentLength;
                for (var i = 0; i < _currentLength; i++) {
                    result = (17*result) + obj[i];
                }
                return result;
            }
        }

        private static byte _currentLength;


        // Currently, on 65534 not distinct words, method works 30(+-4)ms on my core i3-2370m
        public virtual void LoadInto(IDictionary<byte, WordList> words, IList<WholeWord> repository) {
            ushort index = 0;
            var loaded = new HashSet<byte[]>(new TrickyByteArrayEqualityComparer());
            _readAllTask.Wait();

            for (var i = 0; i < _bytes.Length; i++) {
                var firstLetter = _bytes[i];
                if (firstLetter == _separator)
                    continue;

                var buffer = new byte[24];
                buffer[0] = firstLetter;
                _currentLength = 1;

                while (_bytes[++i] != _separator) {
                    buffer[_currentLength++] = _bytes[i];
                }

                if (loaded.Contains(buffer))
                    continue;

                loaded.Add(buffer);

                repository[index] = new WholeWord(buffer, _currentLength);

                WordList wordList;
                if (words.TryGetValue(firstLetter, out wordList))
                    wordList.Add(buffer[_currentLength - 1], index++);
                else {
                    wordList = new WordList();
                    wordList.Add(buffer[_currentLength - 1], index++);
                    words[firstLetter] = wordList;
                }
            }
        }

    }
}