using System.Collections.Generic;
using Citystan.Abstract;
using Citystan.Entities;

namespace Citystan.Services
{
    /// <summary>
    /// Should has all information about available words
    /// </summary>
    public class WordRepository
    {
        private readonly IWordsLoader _loader;
        private readonly IDictionary<byte, WordList> _words;
        private readonly WholeWord[] _allWords;


        public WordRepository(IWordsLoader loader) {
            _loader = loader;
            _words = new Dictionary<byte, WordList>();
            _allWords = new WholeWord[ushort.MaxValue];
            _loader.LoadInto(_words, _allWords);
        }



        public ICollection<byte> AvailableFirstLetters {
            get { return _words.Keys; }
        }




        /// <summary>
        /// Returns list of words, that starts on <paramref name="firstLetter"/>
        /// </summary>
        public virtual WordList WordsOn(byte firstLetter) {
            return _words[firstLetter];
        }


        /// <summary>
        /// Tells, how many words, that starts on <paramref name="firstLetter"/>, are available
        /// </summary>
        public virtual ushort WordsCountOn(byte firstLetter) {
            return _words[firstLetter].Count;
        }


        public WholeWord WordAt(ushort index) {
            return _allWords[index];
        }
    }
}