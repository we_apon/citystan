﻿using System.Collections.Generic;

namespace Citystan.Tests.Helpers
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Returns item, placed at <paramref name="index"/>.
        /// Returns <see langword="default"/>(<typeparamref name="T"/>)
        /// </summary>
        public static T At<T>(this IEnumerable<T> collection, int index) {
            if (index < 0)
                return default(T);

            var current = 0;
            foreach (var item in collection) {
                if (index == current++)
                    return item;
            }

            return default(T);
        } 


    }
}
