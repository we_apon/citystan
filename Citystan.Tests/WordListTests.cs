﻿using System;
using System.Collections.Generic;
using System.Linq;
using Citystan.Entities;
using NBehave.Spec.NUnit;
using NUnit.Framework;

namespace Citystan.Tests
{
    namespace When_working_with_WordList
    {
        class When_working_with_word_list : Specification
        {
            protected WordList List;
            protected ushort ExpectedCount;

            protected override void Establish_context() {
                base.Establish_context();
                List = new WordList();

                var random = new Random();
                ExpectedCount = (ushort)random.Next(20, 50);
                
                for (var i = 0; i < ExpectedCount; i++) {
                    List.Add((byte) random.Next(0, 255), (ushort) i);
                }
            }
        }


        class And_adding_something_to_it : When_working_with_word_list
        {
            [Test]
            public void Words_count_should_be_incremented() {
                var count = List.Count;

                var random = new Random();
                List.Add((byte)random.Next(0, 255), (ushort)random.Next(1000, 65000));

                List.Count.ShouldEqual( (ushort)(count+1) );
            }
        }


        class And_taking_some_word_from_it : When_working_with_word_list
        {
            [Test]
            public void Words_count_should_be_decremented() {
                var count = List.Count;

                var letter = List.LastLetters().First();
                List.GiveMeWordOn(letter);

                List.Count.ShouldEqual( (ushort)(count-1) );
            }
        }



        class And_getting_its_words_count : When_working_with_word_list
        {
            private ushort _count;

            protected override void Because_of() {
                base.Because_of();
                _count = List.Count;
            }

            [Test]
            public void It_should_return_count_of_all_available_words() {
                _count.ShouldEqual(ExpectedCount);
            }
        }



        class And_getting_available_last_letters : When_working_with_word_list
        {
            private IEnumerable<byte> _lastLetters;

            protected override void Because_of() {
                base.Because_of();
                _lastLetters = List.LastLetters();
            }

            [Test]
            public void Last_letters_should_be_returned() {
                _lastLetters.Any().ShouldBeTrue(); // todo: add .ShouldNotBeEmpty extension
            }

            [Test]
            public void Should_be_returnedlast_letters_of_available_words_only() {
                //todo it!
            }

            [Test]
            public void Last_letters_should_be_unique() {
                _lastLetters.Count().ShouldEqual(List.LastLetters().Distinct().Count());
            }
        }
    }
}