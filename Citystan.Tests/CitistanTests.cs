﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Citystan.Entities;
using NBehave.Spec.NUnit;
using NUnit.Framework;

namespace Citystan.Tests
{
    namespace When_running_Citystan_application
    {
        class When_running_citystan_application : Specification
        {
            private Stopwatch _watch;
            private string[] _textChain;
            private long _elapsedMilliseconds;
            private string[] _args = {
                "cities.txt",
                ",",
                WordChain.DefaultOutputPath
            };


            protected override void Because_of() {
                base.Because_of();

                _watch = new Stopwatch();
                _watch.Start();
                Program.Main(_args);
                _elapsedMilliseconds = _watch.ElapsedMilliseconds;

                Console.WriteLine("Currently it runs {0}ms", _elapsedMilliseconds);

                _textChain = File.ReadAllText(WordChain.DefaultOutputPath, Encoding.GetEncoding(1251)).Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            }

            [Test]
            public void It_should_be_pretty_fast() {
                _elapsedMilliseconds.ShouldBeLessThan(120);
            }


            [Test]
            public void Result_should_contain_chain_of_10000_unique_words() {
                _textChain.Count().ShouldEqual(10000);
                _textChain.Distinct().Count().ShouldEqual(10000);
            }


            [Test]
            public void Every_word_in_chain_should_starts_with_last_letter_of_previous_word() {
                var last = default(char);
                foreach (var word in _textChain) {
                    if (last == default(char)) {
                        last = word.Last();
                        continue;
                    }

                    word.First().ShouldEqual(last);
                    last = word.Last();
                }
            }
        }



    }

}
