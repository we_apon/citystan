﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Citystan.Entities;
using Citystan.Services;
using Moq;
using NBehave.Spec.NUnit;
using NUnit.Framework;

namespace Citystan.Tests
{
    namespace When_working_with_ChainFinder
    {
        class When_working_with_chain_finder : Specification
        {
            protected ChainFinder Finder;
            protected Mock<WordChain> Chain;
            protected Mock<WordRepository> Repository;
            protected Mock<FileWordsLoader> Loader;
            protected string CitiesChainFile = "cities-chain.txt";
            protected List<string> TextChain;

            protected override void Establish_context() {
                base.Establish_context();

                Loader = new Mock<FileWordsLoader>("cities.txt", (byte) ',') {CallBase = true};
                Repository = new Mock<WordRepository>(Loader.Object) {CallBase = true};
                Chain = new Mock<WordChain>(Repository.Object, CitiesChainFile) {CallBase = true};
                Finder = new ChainFinder(Repository.Object, Chain.Object);


                TextChain = new List<string>();

                ushort length = 0;
                Chain.Setup(x => x.Length()).Returns(() => length);
                Chain.Setup(x => x.Add(It.IsAny<ushort>())).Callback<ushort>(index => {
                    var word = Repository.Object.WordAt(index);
                    var text = Encoding.GetEncoding(1251).GetString(word.Bytes, 0, word.Length);
                    TextChain.Add(text);
                    length++;
                });
            }
        }


        class And_finding_chain_of_10000_unique_cities : When_working_with_chain_finder
        {
            protected override void Because_of() {
                base.Because_of();
                Finder.FindChain(10000);
            }


            [Test]
            public void Chain_should_be_finded() {
                Chain.Object.Length().ShouldEqual((ushort) 10000);
            }


            [Test]
            public void Chain_should_have_10000_items_length() {
                Chain.Verify(x => x.Add(It.IsAny<ushort>()), Times.Exactly(10000));
            }


            [Test]
            public void Every_word_in_chain_should_starts_with_last_letter_of_previous_word() {
                var last = default (char);
                foreach (var word in TextChain) {
                    if (last == default(char)) {
                        last = word.Last();
                        continue;
                    }

                    word.First().ShouldEqual(last);
                    last = word.Last();
                }
            }


            [Test]
            public void Chain_should_be_unique() {
                TextChain.Count.ShouldEqual(10000);
                TextChain.Distinct().Count().ShouldEqual(10000);
            }
        }
    }
}