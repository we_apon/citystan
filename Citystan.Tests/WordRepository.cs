﻿using System;
using System.Collections.Generic;
using System.Threading;
using Citystan.Abstract;
using Citystan.Entities;
using Citystan.Services;
using Citystan.Tests.Helpers;
using Moq;
using NBehave.Spec.NUnit;
using NUnit.Framework;

namespace Citystan.Tests
{
    namespace When_working_with_WordRepository
    {
        class When_working_with_word_repository : Specification
        {
            protected WordRepository Repository;
            protected Mock<IWordsLoader> Loader;
            protected IDictionary<byte, WordList> Words;
            protected IList<WholeWord> AllWords;

            protected override void Establish_context() {
                base.Establish_context();

                Loader = new Mock<IWordsLoader>();
                Loader.Setup(x => x.LoadInto(It.IsAny<IDictionary<byte, WordList>>(), It.IsAny<IList<WholeWord>>()))
                    .Callback<IDictionary<byte, WordList>, IList<WholeWord>>((words, allWords) => {
                        Words = words;
                        AllWords = allWords;

                        words.Add((byte) 'A', new WordList());
                    });

                Repository = new WordRepository(Loader.Object);
            }
        }


        class Once_it_initialized : When_working_with_word_repository
        {
            [Test]
            public void It_should_start_loading_words_using_word_loader() {
                //? may need to sleep for 1ms, or something..
                Loader.Verify(x => x.LoadInto(It.IsAny<IDictionary<byte, WordList>>(), It.IsAny<IList<WholeWord>>()), Times.Once);
            }
        }




        class And_getting_available_first_letters : When_working_with_word_repository
        {
            private ICollection<byte> _letters;

            protected override void Because_of() {
                base.Because_of();
                Thread.Sleep(50);
                _letters = Repository.AvailableFirstLetters;
            }

            [Test]
            public void We_should_get_keys_of_wordlist_dictionary() {
                _letters.ShouldEqual(Words.Keys);
            }
        }



        class And_getting_words_on_some_letter : When_working_with_word_repository
        {
            private WordList _wordList;
            private byte _firstLetter;

            protected override void Because_of() {
                base.Because_of();
                Thread.Sleep(50);
                var idx = new Random().Next(0, Repository.AvailableFirstLetters.Count);
                _firstLetter = Repository.AvailableFirstLetters.At(idx);
                _wordList = Repository.WordsOn(_firstLetter);
            }


            [Test]
            public void Something_should_be_returned() {
                _wordList.ShouldNotBeNull();
            }

            [Test]
            public void Should_be_returned_appropriate_word_list() {
                _wordList.ShouldBeTheSameAs(Words[_firstLetter]);
            }
        }


    }
}
