﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Citystan.Entities;
using Citystan.Services;
using NBehave.Spec.NUnit;
using NUnit.Framework;

namespace Citystan.Tests
{
    namespace When_working_with_FileWordLoader
    {
        class When_working_with_file_word_loader : Specification
        {
            protected FileWordsLoader Loader;
            protected string[] AllWords;

            protected override void Establish_context() {
                base.Establish_context();
                Loader = new FileWordsLoader("cities.txt", (byte) ',');

                AllWords = File.ReadAllText("cities.txt", Encoding.GetEncoding(1251))
                    .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries);
            }
        }



        class And_loading_some_words_with_it : When_working_with_file_word_loader
        {
            private IDictionary<byte, WordList> _words;
            private WholeWord[] _repository;
            private Encoding _encoding;

            protected override void Because_of() {
                base.Because_of();
                _encoding = Encoding.GetEncoding(1251);
                _words = new Dictionary<byte, WordList>();
                _repository = new WholeWord[ushort.MaxValue];
                Loader.LoadInto(_words, _repository);
            }


            [Test]
            public void Words_should_be_splitted_into_groups_by_first_letter() {
                var expectedKeys = _encoding.GetBytes(AllWords.Distinct().Select(x => x.First()).Distinct().ToArray());
                _words.Keys.OrderBy(x => x).ShouldEqual(expectedKeys.OrderBy(x => x));
            }


            [Test]
            public void WordList_should_has_appropriate_last_letters_and_indexes() {
                foreach (var key in _words.Keys) {
                    var wordList = _words[key];
                    while (wordList.Count > 0) {
                        var lastLetter = wordList.LastLetters().First();
                        var index = wordList.GiveMeWordOn(lastLetter);
                        var wholeWord = _repository[index];
                        wholeWord.Bytes[0].ShouldEqual(key);
                        wholeWord.Bytes[wholeWord.Length - 1].ShouldEqual(lastLetter);

                    }
                }
            }


            [Test]
            public void Should_be_returned_all_unique_words() {
                var count = _repository.Where(x => x.Bytes != null).Count();
                var expectedCount = AllWords.Distinct().Count();
                count.ShouldEqual(expectedCount);


                var strings = _repository.Where(x => x.Bytes != null).Select(x => _encoding.GetString(x.Bytes, 0, x.Length))
                    .OrderBy(x => x).ToArray();

                var expectedStrings = AllWords.Distinct().OrderBy(x => x).ToArray();

                strings.ShouldEqual(expectedStrings);
            }
        }
    }
}